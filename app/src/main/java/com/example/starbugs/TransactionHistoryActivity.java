package com.example.starbugs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.Snapshot;

import android.content.ClipData;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TransactionHistoryActivity extends AppCompatActivity {
    //referensi ke database firebase
    private DatabaseReference mDatabase;
    private List<String> foo = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);




        //chaining listView layout ke dalam variabel
        final ListView itemList = findViewById(R.id.itemLists);



        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,foo);

        itemList.setAdapter(arrayAdapter);



        //mengambil node dalam firebase
         mDatabase = FirebaseDatabase.getInstance().getReference().child("historyPembelian");


         //Query query = mDatabase.orderByChild("namaItem").equalTo("Kentang");


        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    //Items item = snapshot.getValue(Items.class);
                    //System.out.println(item.namaItem);
                    foo.add(snapshot.child("namaItem").getValue().toString() + " harga: " + snapshot.child("hargaItem").getValue().toString());
                    arrayAdapter.notifyDataSetChanged();
                }


                /*
                String value = dataSnapshot.getValue(Items.class).toString();
                foo.add(value);
                arrayAdapter.notifyDataSetChanged();
*/
                /*
                for(DataSnapshot child: dataSnapshot.getChildren()){
                    Items item = child.getValue(Items.class);
                    //foo.add(dataSnapshot.child("deskripsiItem").getValue().toString());
                }*/

                //String name = dataSnapshot.child("deskripsiItem").getValue().toString();
                //test.setText(name);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
