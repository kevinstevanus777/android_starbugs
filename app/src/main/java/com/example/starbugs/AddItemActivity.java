package com.example.starbugs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

//randomizer
import java.math.BigInteger;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddItemActivity extends AppCompatActivity {

    public static final String EXTRA_JENISITEM =  "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);


        String jenisItem  = (String)getIntent().getExtras().get(EXTRA_JENISITEM);
        TextView textHargaItem = findViewById(R.id.textViewHargaText);
        EditText hargaItemView = findViewById(R.id.iHargaItem);

        if(jenisItem.equals("Drink") || jenisItem.equals("Food")){

            textHargaItem.setVisibility(View.VISIBLE);
            hargaItemView.setVisibility(View.VISIBLE);
        }

        //adding listener to the button
        Button btAddNew = (Button)findViewById(R.id.btSubmit);
        btAddNew.setOnClickListener(myHandler1);

    }

    View.OnClickListener myHandler1 = new View.OnClickListener(){

        //ketika btSubmit di klik, akan masuk ke firebase
        public void onClick(View v){

            //mengambil value yang didapat dari mainactivity(value berupa drinks||food||stores)
            String jenisItem  = (String)getIntent().getExtras().get(EXTRA_JENISITEM);


                TextView namaItemView = findViewById(R.id.iNamaItem);
                String namaItem = namaItemView.getText().toString();

                TextView deskripsiItemView = findViewById(R.id.iDeskripsiItem);
                String deskripsiItem = deskripsiItemView.getText().toString();






            //Spinner jenisItemList = findViewById(R.id.dropDownListJenisItem);
            //String jenisItem = jenisItemList.getSelectedItem().toString();




            //panggil database
            DatabaseReference mDatabase;
            mDatabase = FirebaseDatabase.getInstance().getReference();

            //buat primary keynya
            UUID uuid = UUID.randomUUID();

            //insert ke database
            //insert deskripsi item
            mDatabase.child("items").child(uuid.toString()).child("deskripsiItem").setValue(deskripsiItem);
            //insert namaitem
            mDatabase.child("items").child(uuid.toString()).child("namaItem").setValue(namaItem);
            //setjenis itemnya
            mDatabase.child("items").child(uuid.toString()).child("jenisItem").setValue(jenisItem);


            //harga hanya masuk ketika jenisitem adalah drink atau food
            if(jenisItem.equals("Drink") || jenisItem.equals("Food")){

                TextView hargaItemView = findViewById(R.id.iHargaItem);
                Integer hargaItem = Integer.parseInt(hargaItemView.getText().toString());
                //insert hargaItem
                mDatabase.child("items").child(uuid.toString()).child("hargaItem").setValue(hargaItem);

            }


            finish();




        }

    };

    @Override
    public void onDestroy(){
        super.onDestroy();
    }





}
