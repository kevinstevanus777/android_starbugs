package com.example.starbugs;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Items {
    public String jenisItem;
    public String namaItem;
    public String deskripsiItem;
    public Integer hargaItem;




    public Items(){

    }

    public Integer getHargaItem() {
        return hargaItem;
    }

    public void setHargaItem(Integer hargaItem) {
        this.hargaItem = hargaItem;
    }

    public Items(String jenisItem, String namaItem, String deskripsiItem, Integer hargaItem){
        this.jenisItem = jenisItem;
        this.namaItem = namaItem;
        this.deskripsiItem = deskripsiItem;
        this.hargaItem = hargaItem;


    }




    public String getJenisItem() {
        return jenisItem;
    }

    public void setJenisItem(String jenisItem) {
        this.jenisItem = jenisItem;
    }

    public String getNamaItem() {
        return namaItem;
    }

    public void setNamaItem(String namaItem) {
        this.namaItem = namaItem;
    }

    public String getDeskripsiItem() {
        return deskripsiItem;
    }

    public void setDeskripsiItem(String deskripsiItem) {
        this.deskripsiItem = deskripsiItem;
    }
}
