package com.example.starbugs;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.LauncherActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.Tag;

import java.lang.ref.Reference;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListItemActivity extends AppCompatActivity {

//https://stackoverflow.com/questions/40366717/firebase-for-android-how-can-i-loop-through-a-child-for-each-child-x-do-y

    //referensi ke database firebase
    private DatabaseReference mDatabase;

    //berupa list yang bertipe string yang dapat diisi dengan string baru (listIsibarang.add("something"));
    private List<String> listIsiBarang = new ArrayList<String>();
    //ini untuk uuidnya
    private List<String> listIdBarang = new ArrayList<String>();

    //public var untuk dipakai di mainactivity
    public static final String EXTRA_JENISITEM =  "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //membuat instance DO NOT TOUCH
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_item);

        //mengambil value yang didapat dari mainactivity(value berupa drinks||food||stores)
        final String jenisItem  = (String)getIntent().getExtras().get(EXTRA_JENISITEM);

        //chaining listView layout ke dalam variabel
        ListView itemList = findViewById(R.id.itemLists);

        //adding listener to the button
        Button btAddNew = (Button)findViewById(R.id.btAddNew);
        btAddNew.setOnClickListener(myhandler1);

        //linking arrayadapter dengan listView di layout
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listIsiBarang);
        itemList.setAdapter(arrayAdapter);

        //menampilkan list item

        //res: https://www.youtube.com/watch?v=LpWhAz3e1sI
        //final ListView listItems = (ListView) findViewById(R.id.itemLists);

        //mengambil node dalam firebase
        mDatabase = FirebaseDatabase.getInstance().getReference().child("items");

        //membuat filter pada query, dibuat kondisi WHERE jenisItem = jeniItem yang didapat
        Query query = mDatabase.orderByChild("jenisItem").equalTo(jenisItem);


        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                //foreach loop untuk mengambil data
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){

                    //mengambil data dari firebase dan dimasukkan kedalam var item yang merupakan object
                    Items item = snapshot.getValue(Items.class);

                    //melempar namaItem kedalam list barang(ini di foreach jadinya masuk banyak)
                    listIsiBarang.add(item.namaItem);

                    //mengisi id kedalam arrayList nya
                    listIdBarang.add(snapshot.getKey());
                    arrayAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        };


        //memanggil fungsi untuk mengambil dan menmapilkan data ke layar
        query.addListenerForSingleValueEvent(valueEventListener);



        //ini ketika salah satu textView dari listView di klik, akan masuk ke detailItem
        itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ListItemActivity.this,ItemDetailActivity.class);
                intent.putExtra(ItemDetailActivity.EXTRA_ID, (String) listIdBarang.get(position));
                //String jenisItem =(String)getIntent().getExtras().get(EXTRA_JENISITEM);
                //intent.putExtra(ItemDetailActivity.EXTRA_JENISITEM, (String) jenisItem); MASALAH DISINI
                startActivity(intent);
            }
        });


    }

    //soruce: https://stackoverflow.com/questions/3320115/android-onclicklistener-identify-a-button
    //listener untuk btaddnew
    View.OnClickListener myhandler1 = new View.OnClickListener(){
        public void onClick(View v){
            Intent intent = new Intent(ListItemActivity.this, AddItemActivity.class);
            intent.putExtra(AddItemActivity.EXTRA_JENISITEM, (String) getIntent().getExtras().get(EXTRA_JENISITEM));
            startActivity(intent);
        }
    };



    public void onRestart(){
        super.onRestart();
        finish();
        startActivity(getIntent());
    }



}
