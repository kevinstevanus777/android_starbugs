package com.example.starbugs;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.LauncherActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<String> listFavorite = new ArrayList<String>();

    private static final String TAG = "MainActivity";
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupOptionListView();

        //addNewDrink("1","Kopi","Kopi Dijalan");


        //adding listener to the button
        Button btAddNew = (Button)findViewById(R.id.btHistory);
        btAddNew.setOnClickListener(myhandler1);




        ListView listItems = (ListView) findViewById(R.id.favoriteList);

        //linking arrayadapter dengan listView di layout
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listFavorite);
        listItems.setAdapter(arrayAdapter);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("items");
        //membuat filter pada query, dibuat kondisi WHERE jenisItem = jeniItem yang didapat
        Query query = mDatabase.orderByChild("favorite").equalTo("yes");

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //foreach loop untuk mengambil data
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){

                    //mengambil data dari firebase dan dimasukkan kedalam var item yang merupakan object
                    Items item = snapshot.getValue(Items.class);

                    //melempar namaItem kedalam list barang(ini di foreach jadinya masuk banyak)
                    listFavorite.add(item.namaItem);


                    arrayAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        query.addListenerForSingleValueEvent(valueEventListener);
    }



    private void setupOptionListView() {
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override


            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                //this for drinks
                if (i == 0) {
                    Intent intent = new Intent(MainActivity.this, ListItemActivity.class);
                    intent.putExtra(ListItemActivity.EXTRA_JENISITEM, (String) "Drink");
                    startActivity(intent);
                }
                //this for food

                else if(i == 1){

                    Intent intent = new Intent(MainActivity.this, ListItemActivity.class);
                    intent.putExtra(ListItemActivity.EXTRA_JENISITEM, (String) "Food");
                    startActivity(intent);
                }
                else if(i == 2){

                    Intent intent = new Intent(MainActivity.this,ListItemActivity.class);
                    intent.putExtra(ListItemActivity.EXTRA_JENISITEM, (String) "Stores");
                    startActivity(intent);
                }
            }




        };

        ListView listView = (ListView) findViewById(R.id.menu_options);
        listView.setOnItemClickListener(itemClickListener);
    }



    //soruce: https://stackoverflow.com/questions/3320115/android-onclicklistener-identify-a-button
    //listener untuk btaddnew
    View.OnClickListener myhandler1 = new View.OnClickListener(){
        public void onClick(View v){
            Intent intent = new Intent(MainActivity.this, TransactionHistoryActivity.class);

            startActivity(intent);
        }
    };


}
