package com.example.starbugs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.UUID;


public class ItemDetailActivity extends AppCompatActivity {

    //referensi ke database firebase
    private DatabaseReference mDatabase;

    public static final String EXTRA_ID =  "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        //primary key dari firebase
        String keyItem  = (String)getIntent().getExtras().get(EXTRA_ID);



        //mencari layoutnya
        final TextView namaItemView = findViewById(R.id.name);
        final TextView deskripsiItemView = findViewById(R.id.description);
        final TextView hargaItemView = findViewById(R.id.hargaItem);

        //mengambil node dalam firebase
        mDatabase = FirebaseDatabase.getInstance().getReference().child("items").child(keyItem);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                namaItemView.setText(dataSnapshot.child("namaItem").getValue().toString());
                deskripsiItemView.setText(dataSnapshot.child("deskripsiItem").getValue().toString());

                try{
                    hargaItemView.setText(dataSnapshot.child("hargaItem").getValue().toString());

                }
                catch (Exception e){
                    System.out.println(e);
                    return;
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    public void onFavouriteClicked(View view) {
        //primary key dari firebase
        String keyItem  = (String)getIntent().getExtras().get(EXTRA_ID);


        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("items").child(keyItem).child("favorite").setValue("yes");
    }

    public void onBuyClicked(View view){

        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //buat primary keynya
        UUID uuid = UUID.randomUUID();
        final TextView namaItemView = findViewById(R.id.name);
        String namaItem = (String) namaItemView.getText();

        final TextView hargaItemView = findViewById(R.id.hargaItem);
        String hargaItem = (String) hargaItemView.getText();



        mDatabase.child("historyPembelian").child(uuid.toString()).child("jenisItem").setValue("");
        mDatabase.child("historyPembelian").child(uuid.toString()).child("deskripsiItem").setValue("");
        mDatabase.child("historyPembelian").child(uuid.toString()).child("namaItem").setValue(namaItem);
        mDatabase.child("historyPembelian").child(uuid.toString()).child("hargaItem").setValue(hargaItem);
    }
}
